package com.example.codemate.downloadimagewithpicasso

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class MainActivity : AppCompatActivity() {
    private val imageUrl = "https://www.adventurouskate.com/wp-content/uploads/2017/10/DSCF8129.jpg"

    private val imageDir = "imageDir"
    private val imageName = "my_image.png"
    private var ivImage: ImageView? = null
    private var tvLog: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ivImage = findViewById<View>(R.id.iv_image) as ImageView
        tvLog = findViewById<View>(R.id.tv_log) as TextView
    }

    private fun log(logStr: String) {
        Log.d("MainActivity", logStr)
        tvLog!!.text = logStr
    }

    /**********************************************************************************************************
     * Button onClick methods
     */
    fun loadImageFromUrl(v: View) {
        log("Load image from url")
        Picasso.with(this).load(imageUrl).into(ivImage)
    }

    fun downloadSaveImageFromUrl(v: View) {
        log("Load image from url and save it to disk through Picasso")
        Picasso.with(this).load(imageUrl).into(picassoImageTarget(applicationContext, imageDir, imageName))
    }

    fun loadImageFromDisk(v: View) {
        val cw = ContextWrapper(applicationContext)
        val directory = cw.getDir(imageDir, Context.MODE_PRIVATE)
        val myImageFile = File(directory, imageName)

        if (myImageFile.exists()) {
            log("Load image from disk: " + myImageFile.absolutePath)
            Picasso.with(this).load(myImageFile).into(ivImage)

        } else {
            ivImage!!.setImageBitmap(null) // remove the existing image first
            log("The image doesn't exist on disk!")
        }
    }

    fun deleteImageFromDisk(v: View) {
        val cw = ContextWrapper(applicationContext)
        val directory = cw.getDir(imageDir, Context.MODE_PRIVATE)
        val myImageFile = File(directory, imageName)

        if (myImageFile.exists()) {
            if (myImageFile.delete())
                log("The image on the disk deleted successfully!")
            else
                log("Failed to delete " + myImageFile.absolutePath)
        } else {
            log("No such image on disk! No need to delete it!")
        }
    }

    fun checkIfImageExist(v: View) {
        val cw = ContextWrapper(applicationContext)
        val directory = cw.getDir(imageDir, Context.MODE_PRIVATE)
        val myImageFile = File(directory, imageName)

        if (myImageFile.exists())
            log("the image is there!")
        else
            log("The image is not there!")
    }

    /**********************************************************************************************************
     * Target class for saving image bitmap returned from Picasso
     */
    private fun picassoImageTarget(context: Context, imageDir: String, imageName: String): Target {
        Log.d("picassoImageTarget", " picassoImageTarget")
        val cw = ContextWrapper(context)
        val directory = cw.getDir(imageDir, Context.MODE_PRIVATE) // path to /data/data/yourapp/app_imageDir
        return object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                Thread(Runnable {
                    val myImageFile = File(directory, imageName) // Create image file
                    var fos: FileOutputStream? = null
                    try {
                        fos = FileOutputStream(myImageFile)
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } finally {
                        try {
                            fos!!.close()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }

                    runOnUiThread { log("image saved to >>>" + myImageFile.absolutePath) }
                }).start()
            }

            override fun onBitmapFailed(errorDrawable: Drawable) {
                Log.e("Bitmap Failed",""+errorDrawable)
            }
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                if (placeHolderDrawable != null) {
                }
            }
        }
    }
}
